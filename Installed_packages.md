# List of installed packages

## All explicitly installed packages

created with `pacman -Qeq`

```
apparmor
autoconf
automake
base
bash-completion
binutils
bison
btrfs-progs
cpupower
dirvish
docker
docker-compose
efibootmgr
flex
fuse2
gcc
git
grub
hdparm
htop
inetutils
intel-ucode
linux
linux-firmware
linux-lts
lynx
m4
make
man-db
man-pages
mmv
nano
nftables
openssh
pacman-contrib
parted
patch
perl-time-parsedate
perl-time-period
pkgconf
plocate
polkit
powertop
rsync
screen
smartmontools
sudo
texinfo
unzip
usbutils
wget
which
yay
```

## Packages from Arch aur

created with `pacman -Qentq`

```
apparmor
autoconf
automake
base
bison
cpupower
docker
docker-compose
flex
gcc
grub
hdparm
htop
inetutils
intel-ucode
linux-lts
lynx
make
man-db
man-pages
nano
nftables
openssh
pacman-contrib
parted
patch
pkgconf
powertop
screen
smartmontools
texinfo
unzip
usbutils
wget
```

## 3<sup>rd</sup> party packages

createdw with `pacman -Qmq`

```
dirvish
mmv
perl-time-parsedate
perl-time-period
yay
```
